@echo off

echo Hi, today %date%
pause

echo Begin copy data

set source_folder="D:\backup"
set destination_folder="E:\backup"

xcopy "%source_folder%" "%destination_folder%" /s /e /h /y

echo Done copy data

pause